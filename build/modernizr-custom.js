/* Modernizr (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-forms_placeholder-shiv-load-cssclasses
 */
;Modernizr.addTest("placeholder",function(){return"placeholder"in(Modernizr.input||document.createElement("input"))&&"placeholder"in(Modernizr.textarea||document.createElement("textarea"))});