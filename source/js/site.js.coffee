$(document).on "ready", ->
  fieldDay.init()

fieldDay =
  equalHeight: (group) ->
    tallest = 0
    group
      .height("auto")
      .each ->
        thisHeight = $(this).height()
        if thisHeight > tallest
          tallest = thisHeight
      .height(tallest)

  onLoad: ->
    fieldDay.onResize()

  onResize: ->

  init: ->
    fieldDay.onLoad()

    $('.js-slider').cycle
      log: false
      manualSpeed: 500
      next: '.js-slider-next'
      pager: '> .slider-pager'
      pagerActiveClass: 'is-current'
      prev: '.js-slider-prev'
      slideActiveClass: 'is-current'
      slides: '> img'
      speed: 500

class fieldDay.Controller
