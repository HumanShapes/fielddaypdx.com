(function() {
  var fieldDay;

  $(document).on("ready", function() {
    return fieldDay.init();
  });

  fieldDay = {
    equalHeight: function(group) {
      var tallest;
      tallest = 0;
      return group.height("auto").each(function() {
        var thisHeight;
        thisHeight = $(this).height();
        if (thisHeight > tallest) {
          return tallest = thisHeight;
        }
      }).height(tallest);
    },
    onLoad: function() {
      return fieldDay.onResize();
    },
    onResize: function() {},
    init: function() {
      fieldDay.onLoad();
      return $('.js-slider').cycle({
        log: false,
        manualSpeed: 500,
        next: '.js-slider-next',
        pager: '> .slider-pager',
        pagerActiveClass: 'is-current',
        prev: '.js-slider-prev',
        slideActiveClass: 'is-current',
        slides: '> img',
        speed: 500
      });
    }
  };

  fieldDay.Controller = (function() {
    function Controller() {}

    return Controller;

  })();

}).call(this);
