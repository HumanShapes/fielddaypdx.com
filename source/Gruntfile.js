/*global module:false*/
module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    modernizr: {
      dist: {
        "devFile" : "remote",
        "outputFile" : "../build/modernizr-custom.js",
        "extra" : {
          "shiv" : true,
          "printshiv" : false,
          "load" : true,
          "mq" : false,
          "cssclasses" : true
        },
        "extensibility" : {
          "addtest" : false,
          "prefixed" : false,
          "teststyles" : false,
          "testprops" : false,
          "testallprops" : false,
          "hasevents" : false,
          "prefixes" : false,
          "domprefixes" : false
        },
        "uglify" : true,
        "tests" : [
          "forms_placeholder"
        ],
        "parseFiles" : true,
        // When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
        // You can override this by defining a "files" array below.
        // "files" : {
            // "src": []
        // },
        "matchCommunityTests" : false,
        "customTests" : []
      }
    },
    svgmin: {
      files: {
        expand: true,
        cwd: 'svgs',
        src: ['*.svg'],
        dest: 'svgs/minified/',
      }
    },
    sass: {
      build: {
        options: {
          style: 'compressed',
          banner: 'This file was generated from the SCSS files within the source directory. Do not edit directly - use the build process.'
        },
        files: {
          '../build/style.css': ['css/site.scss']
        }
      }
    },
    coffee: {
      compile: {
        files: {
          'js/site.js': 'js/site.js.coffee'
        }
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
          'bower_components/jquery/dist/jquery.js',
          'bower_components/respond/dest/respond.src.js',
          'bower_components/jquery-cycle2/build/jquery.cycle2.js',
          'js/site.js'
        ],
        dest: '../build/compiled.js'
      }
    },
    watch: {
      css: {
        options: {
          interrupt: true,
          livereload: true
        },
        files: ['css/**/*.scss'],
        tasks: ['sass']
      },
      js: {
        options: {
          interrupt: true
        },
        files: ['js/*.coffee'],
        tasks: ['js']
      },
      pages: {
        options: {
          interrupt: true
        },
        files: ['pages/**/*'],
        tasks: ['includes']
      }
    },
    includes: {
      options: {
        debug: false
      },
      files: {
        src: ['index.html'],
        dest: '../build/',
        cwd: 'pages/',
        flatten: true
      }
    },
    copy: {
      misc: {
        files: [
          {expand: true, src: ['downloads/**/*'], dest: '../build/'},
          {expand: true, src: ['fonts/**/*'], dest: '../build/'},
          {expand: true, src: ['icons/**/*'], dest: '../build/'},
          {expand: true, src: ['favicons/**/*'], dest: '../build/'}
        ]
      }
    },
    uglify: {
      build: {
        files: {
          '../build/compiled.min.js' : '../build/compiled.js'
        }
      }
    },
    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'images/',
          src: ['*.{png,jpg,gif}'],
          dest: '../build/images/'
        }]
      }
    }
  });

  grunt.loadNpmTasks('grunt-favicons');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-svgmin');
  grunt.loadNpmTasks('grunt-grunticon');
  grunt.loadNpmTasks("grunt-modernizr");
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-aws-s3');

  grunt.registerTask('js', ['coffee', 'concat', 'uglify']);
  grunt.registerTask('all', ['modernizr', 'icons', 'copy', 'includes', 'sass', 'js', 'imagemin']);
  grunt.registerTask('default', ['copy', 'sass', 'js']);
  grunt.registerTask('deploy', ['s3:production']);
};
